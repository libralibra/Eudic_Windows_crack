# Eudic_Windows_crack

官方欧路词典V13.6.2版本是我上传的eudic_win.zip，此处的eudic_win.zip是备份官方的这个版本(需要破解的版本)安装包。

Windows版欧路词典破解文件已经完全上传，至此跟着下面视频中的方法完全可以破解 Windows 版欧路词典了。

破解方法见  https://youtu.be/e62Fii9i6dk

# 重要提醒

这个代码仓库该备份的赶紧备份，该下载的赶紧下载。

# 关于我这个破解是否带病毒？

对于电脑小白用户来说，不太容易辨别破解文件是否带病毒，很简单，eudic-active.reg 这个文件把后缀 .reg 改为 .txt 看看里面的内容，看完后再把后缀改为原来的 .reg

对于 eudic_win.zip 里面的exe是官方安装包，不放心的话可以去各个查毒平台查下看看有没有毒，

对于 eudic-active.exe 不放心的话也可以去各个查毒平台查下看看有没有毒。

还是不放心？ 先把软件安装在虚拟机里运行，或者安装的时候打开微软自带的杀毒软件(其他你信的过的杀毒软件也可以)

# 我是Macos用户，怎么使用破解版欧路词典？

Windows用户可以用破解版，Macos用户也可以用，苹果系统的欧路词典我也破解成功，见 https://github.com/NiceDayk/Eudic-Macos-Crack

Macos破解欧路词典相关的，后续很快我也会放出来。
